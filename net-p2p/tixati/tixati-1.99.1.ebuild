# Copyright 2015 Chun-Yu Lee (Mat) <matlinuxer2@gmail.com>
# Distributed under the terms of the MIT License
# $Header: $

EAPI=5

inherit eutils user versionator

DESCRIPTION="Tixati is a New and Powerful P2P System."
HOMEPAGE="http://www.tixati.com/"
MY_PV=$(replace_version_separator 2 '-')
SRC_URI="
    x86? ( http://www.tixati.com/download/tixati-${MY_PV}.i686.manualinstall.tar.gz )
	amd64? ( http://www.tixati.com/download/tixati-${MY_PV}.x86_64.manualinstall.tar.gz )"

LICENSE="Tixati"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""
RESTRICT="mirror"
RDEPEND=""

src_unpack() {
	unpack ${A}
	S="$( find ${WORKDIR} -type d -iname 'tixati-*.manualinstall' | head -1 )"
}

src_install() {
	local targetdir="/opt/tixati"

	insinto "${targetdir}"
	doins -r *
	fperms a+x "${targetdir}"/tixati
	dosym "/opt/tixati/tixati" "/usr/bin/tixati"
}
