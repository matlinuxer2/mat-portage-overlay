# Copyright
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5

inherit cmake-utils multilib

if [[ ${PV} == "9999" ]] ; then
	EGIT_REPO_URI="https://github.com/apple/${PN}.git"
	inherit git-r3
else
	SRC_URI="https://github.com/apple/${PN}/archive/swift-${PV}-RELEASE.tar.gz -> ${P}.tar.gz"
	KEYWORDS="amd64 x86"
fi

DESCRIPTION="The Swift Programming Language"
HOMEPAGE="https://swift.org/"

LICENSE="Apache-2.0"
SLOT="0"
IUSE=""

RDEPEND="
"
#!libressl? ( dev-libs/openssl:0 )
#libressl? ( dev-libs/libressl )
#sys-libs/zlib
#net-libs/http-parser:=
#gssapi? ( virtual/krb5 )
#ssh? ( net-libs/libssh2 )

DEPEND="${RDEPEND}
	sys-devel/clang
	dev-haskell/cmark
	virtual/pkgconfig
"

DOCS=( CHANGELOG.md CMakeLists.txt CODE_OWNERS.TXT CONTRIBUTING.md LICENSE.txt README.md )

src_unpack() {
	unpack ${A}
	local S0=$( find ${WORKDIR} -maxdepth 1 -type d -iname "${PN}*" | head -1 )
	mv -v ${S0} ${S}
}

src_prepare() {
	cmake-utils_src_prepare
}

src_configure() {
	: #cmake-utils_src_configure
}

src_compile() {
	utils/build-script -R -T --llbuild --swiftpm --xctest --foundation -- --reconfigure
}

src_test() {
	cmake-utils_src_test
}

src_install() {
	cmake-utils_src_install
}
