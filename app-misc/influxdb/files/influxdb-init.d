#!/sbin/runscript
# Copyright (c) 2015-2015 Chun-Yu Lee (Mat) <matlinuxer2@gmail.com>
# Released under the MIT license.

description="InfluxDB is a time series, metrics, and analytics database."

: ${INFLUXDB_PIDFILE:='/run/influxdb.pid'}
: ${INFLUXDB_CFGFILE:='/opt/influxdb/shared/config.toml'}
: ${INFLUXDB_NICELVL:='5'}
: ${INFLUXDB_USER:='influxdb'}

set_num_fd(){
	if [ "x$NOFILES" == "x" ]; then
	    NOFILES=0
	fi

	if [ $NOFILES -le 0 ]; then
	    NOFILES=65536
	fi

	if [ "x$STDOUT" == "x" ]; then
	    STDOUT=/dev/null
	fi

	echo "Setting ulimit -n $NOFILES"
	if ! ulimit -n $NOFILES >/dev/null 2>&1; then
	    echo -n "Cannot set the max number of open file descriptors"
	fi
}

depend() {
	need root
}


start() {
	local startup_early_timeout=${STARTUP_EARLY_TIMEOUT:-1000}

	ebegin "Starting influxdb"
	set_num_fd
	start-stop-daemon --start --user "${INFLUXDB_USER}:influxdb" \
		--background \
		--wait ${startup_early_timeout} \
		--quiet --make-pidfile --pidfile "${INFLUXDB_PIDFILE}" \
		--nicelevel "${INFLUXDB_NICELVL}" --exec /usr/bin/influxdb -- \
		        -config "${INFLUXDB_CFGFILE}"
	eend $? "Failed to start influxdb"
}

stop() {
	local stop_timeout=${STOP_TIMEOUT:-120}

	ebegin "Stopping influxdb"
	start-stop-daemon --stop \
		--retry ${stop_timeout} \
		--quiet --pidfile "${INFLUXDB_PIDFILE}"
	eend $? "Failed to stop influxdb"
}
