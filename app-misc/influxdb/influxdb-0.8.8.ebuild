# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/net-misc/dropbox/dropbox-2.8.4.ebuild,v 1.1 2014/07/08 00:23:12 naota Exp $

EAPI=5

inherit eutils user

DESCRIPTION="InfluxDB is a time series, metrics, and analytics database."
HOMEPAGE="http://influxdb.com/"
SRC_URI="
    x86? ( http://s3.amazonaws.com/influxdb/influxdb-${PV}.386.tar.gz )
	amd64? ( http://s3.amazonaws.com/influxdb/influxdb-${PV}.amd64.tar.gz )"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""
RESTRICT="mirror"
RDEPEND=""

src_unpack() {
	unpack ${A}
	S="${WORKDIR}/build"
}

pkg_setup() {
	enewgroup influxdb
	enewuser influxdb -1 -1 /opt/influxdb influxdb
}

src_install() {
	local targetdir="/opt/influxdb/versions/${PV}"

	insinto "${targetdir}"
	doins -r *
	fperms a+x "${targetdir}"/influxdb
	fperms a+x "${targetdir}"/influxdb-benchmark
	fperms a+x "${targetdir}"/scripts/influxdb-daemon.sh
	dosym "versions/${PV}" "${targetdir}/../../current"
	newinitd "${FILESDIR}/influxdb-init.d" influxdb

	dosym "/opt/influxdb/current/influxdb" "/usr/bin/influxdb"
	dosym "/opt/influxdb/current/influxdb-benchmark" "/usr/bin/influxdb-benchmark"
	dosym "/opt/influxdb/current/scripts/influxdb-daemon.sh" "/usr/bin/influxdb-daemon"

	keepdir ${targetdir} &&
		fowners influxdb:influxdb ${targetdir} &&
		fperms 750 ${targetdir}
}
